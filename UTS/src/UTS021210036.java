
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Aji Andriawan
 */
public class UTS021210036 {

    public static void main(String[] args) {
        //inisialisasi
        Scanner input = new Scanner(System.in);
        String nama,prodi;
        String grade = null;
        int npm;
        //input
        System.out.println("=====Project UTS Algoritma=====");
        System.out.println("===============================");
        
        System.out.println("Silahkan Masukkan Nama Anda  : ");
        System.out.print("-> ");
        nama = input.nextLine();
               
        System.out.println("Silahkan Masukkan Prodi Anda : ");
        System.out.print("-> ");
        prodi = input.nextLine();
        
        System.out.println("Silahkan Masukan Npm Anda    : ");
        System.out.print("-> ");
        npm = input.nextInt();
       
        System.out.println("Silahkan Masukan Nilai Anda  : ");
        System.out.print("-> ");
        int nilai = input.nextInt();
        
        if(nilai>=70){
            grade = "Lulus";
        }else if(nilai<70){
            grade = "Gagal";
        }
        System.out.println("===============================");
        System.out.println("=======Data Inputan Anda=======");
        System.out.println("===============================");
        System.out.println("Nama    : " + nama);
        System.out.println("Prodi   : " + prodi);
        System.out.println("NPM     : " + npm);
        System.out.println("Nilai   : " + nilai);
        System.out.println("===============================");
        System.out.println("Grade   : " + grade);
        System.out.println("===============================");
    }
}
